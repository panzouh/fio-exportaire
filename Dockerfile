FROM golang:1.16.3 AS build
WORKDIR /src
COPY . .
RUN go build -o /build

FROM ubuntu:20.04 as app
ENV buffer_size=2300
RUN apt update && apt install fio -y
RUN apt clean
RUN rm -rf /var/cache/apt/archives
COPY --from=build /build /

ENTRYPOINT ["/build"]

EXPOSE 1334