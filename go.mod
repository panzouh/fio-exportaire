module main

go 1.16

require (
	github.com/gorilla/mux v1.8.0 // direct
	github.com/prometheus/client_golang v1.10.0 // direct
)
