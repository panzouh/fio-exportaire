package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	fioCmd = "fio --rw=write --ioengine=sync --fdatasync=1 --directory=/tmp --name=fio-test"

	promRegistry = prometheus.NewRegistry()

	defaultInt = 15 * time.Minute

	fioNinetyNineDotNinePercentile = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "fio_ninety_nine_dot_nine_percentile",
		Help: "FIO 99.900000th percentile (in ms)",
	})
	fioNinetyNineDotNinetyFivePercentile = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "fio_ninety_nine_dot_ninetyfive_percentile",
		Help: "FIO 99.950000th percentile (in ms)",
	})
	fioNinetyNineDotNinetyNinePercentile = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "fio_ninety_nine_dot_ninetynine_percentile",
		Help: "FIO 99.990000th percentile (in ms)",
	})
	fioWriteBW = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "fio_write_bandwidth_kbps",
		Help: "Write bandwidth measured by FIO (in KiB/s)",
	})
	fioBenchmarkDurationMS = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "fio_benchmark_duration_ms",
		Help: "Duration of last successful benchmark (in ms)",
	})
	fioBenchmarkSuccess = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "fio_benchmark_success",
		Help: "1 if last benchmark was successful, 0 otherwise",
	})
)

func prometheusHandlerError(err error) {
	log.Printf("Error %s\n", err)
	fioBenchmarkSuccess.Set(0)
}

func prometheusHandlerSuccess(fioNineNineNine, fioNineNineNineFive, fioNineNineNineNine, writeBW float64, duration time.Duration) {
	fioBenchmarkSuccess.Set(1)
	fioWriteBW.Set(writeBW)
	fioNinetyNineDotNinePercentile.Set(fioNineNineNine)
	fioNinetyNineDotNinetyFivePercentile.Set(fioNineNineNineFive)
	fioNinetyNineDotNinetyNinePercentile.Set(fioNineNineNineNine)
	fioBenchmarkDurationMS.Set(float64(duration / time.Millisecond))
}

func init() {
	promRegistry.MustRegister(
		fioNinetyNineDotNinePercentile,
		fioNinetyNineDotNinetyFivePercentile,
		fioNinetyNineDotNinetyNinePercentile,
		fioWriteBW,
		fioBenchmarkDurationMS,
		fioBenchmarkSuccess,
	)
}

func Healtz(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintln(w, "OK")
}

func main() {
	r := mux.NewRouter()

	bufferSize := os.Getenv("buffer_size")
	calculatedEtcdDataSize, _ := strconv.ParseFloat(strings.TrimSpace(bufferSize), 64)
	etcdDataSize := calculatedEtcdDataSize * 0.01

	fioCmd := fioCmd + " --bs=" + bufferSize + " --size=" + fmt.Sprint(etcdDataSize) + "mb"
	log.Println(fioCmd)

	duration := flag.Duration("interval", defaultInt, "default interval for consecutive benchmarks runs")
	flag.Parse()
	log.Printf("Configured interval: %v\n", duration)
	go func() {
		ch := make(chan struct{}, 1)
		ch <- struct{}{}

		for {
			<-ch
			time.AfterFunc(*duration, func() { ch <- struct{}{} })
			log.Printf("Running fio: %s", fioCmd)

			start := time.Now()
			cmd := fmt.Sprintf("%s --minimal", fioCmd)
			cmdParts := strings.Split(cmd, " ")
			output, err := exec.Command(cmdParts[0], cmdParts[1:]...).Output()
			if err != nil {
				prometheusHandlerError(err)
				continue
			}
			fioBenchmarkSuccess.Set(1)
			s := strings.TrimSuffix(string(output), "\n")
			parts := strings.Split(s, ";")
			d := time.Now().Sub(start)
			log.Printf("Benchmark finished after %v: %v", d, parts)

			fioNineNineNine, err := strconv.ParseFloat((strings.TrimPrefix(parts[72], "99.900000%=")), 64)
			if err != nil {
				prometheusHandlerError(err)
				continue
			}
			fioNineNineNineFive, err := strconv.ParseFloat((strings.TrimPrefix(parts[73], "99.950000%=")), 64)
			if err != nil {
				prometheusHandlerError(err)
				continue
			}
			fioNineNineNineNine, err := strconv.ParseFloat((strings.TrimPrefix(parts[74], "99.990000%=")), 64)
			if err != nil {
				prometheusHandlerError(err)
				continue
			}
			writeBW, err := strconv.ParseFloat(parts[47], 64)
			if err != nil {
				prometheusHandlerError(err)
				continue
			}
			prometheusHandlerSuccess(fioNineNineNine, fioNineNineNineFive, fioNineNineNineNine, writeBW, d)
		}
	}()

	// Index
	r.Path("/").Handler(http.FileServer(http.Dir("./static/")))
	r.Handle("/img/{rest}",
		http.StripPrefix("/img/", http.FileServer(http.Dir("./static/img/"))),
	)

	// Liveness Probe
	r.HandleFunc("/healthz", Healtz)
	r.Handle("/healthz", r)

	// Prometheus
	r.Path("/metrics").Handler(promhttp.HandlerFor(
		promRegistry,
		promhttp.HandlerOpts{
			EnableOpenMetrics: true,
		},
	))
	r.Path("/obj/{id}").HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {},
	)

	// Serve App
	http := &http.Server{Addr: "0.0.0.0:1334", Handler: r}
	log.Println("Listening on ::1334")
	err := http.ListenAndServe()
	log.Fatal(err)
}
